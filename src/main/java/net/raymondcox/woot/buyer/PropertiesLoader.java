package net.raymondcox.woot.buyer;

import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesLoader {
	private static PropertiesLoader instance = new PropertiesLoader();
	private final Logger logger = Logger.getLogger(getClass());
	
	private PropertiesLoader() {
	}
	
	public static PropertiesLoader getInstance() {
		return instance;
	}
	
	public Properties loadProperties(String propertiesFile) {
		 logger.info("Loading " + propertiesFile);
		 try {
		 	 InputStream in = getClass().getResourceAsStream("/" + propertiesFile);
		 	 Properties properties = new Properties();
			 properties.load(in);
			 in.close();
			 return properties;
		 } catch (Exception ex) {
			 return null;
		 }
	 }

}
