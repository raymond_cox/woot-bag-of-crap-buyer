package net.raymondcox.woot.buyer;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		final Logger logger = Logger.getLogger(Main.class);
		final String CONFIG_FILE = "config.properties";
		final Properties props = PropertiesLoader.getInstance().loadProperties(CONFIG_FILE);
		if (props == null) {
			throw new RuntimeException("Unable to load " + CONFIG_FILE);
		}
        final String username = props.getProperty("username");
        final String password = props.getProperty("password");
        if (username == null) throw new RuntimeException("username not found in " + CONFIG_FILE);
        if (password == null) throw new RuntimeException("password not found in " + CONFIG_FILE);
        final int refreshTime = Integer.valueOf(props.getProperty("refresh.time", "5000"));
		
        WebDriver driver = new HtmlUnitDriver();
        logger.info("Signing into woot as " + username);
        driver.navigate().to("https://account.woot.com/signin");
        driver.findElement(By.id("username")).sendKeys(username);
        driver.findElement(By.id("password")).sendKeys(password);
        driver.findElement(By.className("primary-button")).click();
        
        while (true) {
        	driver.navigate().to("http://www.woot.com");
	        WebElement wantElement = driver.findElement(By.className("wantone"));
	        String itemName = driver.findElement(By.className("fn")).getText();
	        logger.info("Item for sale: " + itemName);
	        if (itemName.equals("Bag of Crap") || itemName.equals("Random Crap")) {
	        	wantElement.click();
	            WebElement buyElement = driver.findElement(By.className("buy-now"));
	            buyElement.click();
	            WebElement placeElement = driver.findElement(By.id("place-order"));
	            placeElement.click();
	        }
	        try {
				Thread.sleep(refreshTime);
			} catch (InterruptedException e) {
				logger.error(e);
			}
        }
		
	}

}
