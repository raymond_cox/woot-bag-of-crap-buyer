How to use
============

1. Download [Maven](http://maven.apache.org) and add it to your path environment variable

2. Git the source 

   `git clone https://github.com/coxry/Woot-BOC-Buyer.git`
   
3. Import maven project from eclipse

4. Create your own src/main/resources/config.properties based off of the sample.config.properties
